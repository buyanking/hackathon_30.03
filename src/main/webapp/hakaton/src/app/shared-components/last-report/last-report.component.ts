import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ReportObj} from '../../model/ReportObj';
import {LevelEnun} from '../../model/LevelEnum';
import {WeekControlService} from '../../services/week-control.service';


// Последние отчеты по проекту
@Component({
  selector: 'app-last-report',
  templateUrl: './last-report.component.html',
  styleUrls: ['./last-report.component.css']
})
export class LastReportComponent implements OnInit {
  @Input() reports: ReportObj;
  @Input() isSpinner: boolean;
  @Output() srcollEvent = new EventEmitter();

  constructor(
    public weekControl: WeekControlService
  ) { }

  ngOnInit() {
    console.log(this.reports);
  }

  // Цвет отчета исходя из цветов каждой оценки (наихудшее)
  getReportGradient(report: ReportObj): string {
    if (report.customerSatisfaction == LevelEnun.BAD || report.successOfTheProject == LevelEnun.BAD
      || report.teamCompetence == LevelEnun.BAD || report.teamMotivation == LevelEnun.BAD) {
      return 'bad';
    } else if (report.customerSatisfaction == LevelEnun.AVERAGE || report.successOfTheProject == LevelEnun.AVERAGE
      || report.teamCompetence == LevelEnun.AVERAGE || report.teamMotivation == LevelEnun.AVERAGE){
      return 'average';
    } else {
      return 'good';
    }
  }

  // подгрузка следующей страницы только если открыт блок
  onScroll() {
    if ($('#last-reports > ul > li').hasClass('active')) {
      this.srcollEvent.emit();
    }
  }
}
