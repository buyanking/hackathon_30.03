import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {SelectObj} from '../../model/SelectObj';
import {Pageable} from '../../model/Pageable';
import {HttpClient} from '@angular/common/http';

// Кастомный селект, основанный на MzSelect
@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css']
})
export class SelectComponent implements OnInit {
  @Input() selectObj: SelectObj;
  @Output() selectChange = new EventEmitter();
  timeOut;
  data: any;
  selectedValue: string;

  totalPages: number;
  currentPage: number = 0;
  isPending: boolean = false; // Загружается ли следующая страница

  constructor( private http: HttpClient) { }

  ngOnInit() {
    if (this.selectObj.url) {
      this.data = [];
      this.loadData();
      if (this.selectObj.data[0]) {
        this.selectedValue = this.selectObj.data[0];
        this.data.push(this.selectObj.data[0]);
      }
    }
  }

  private loadData() {
    this.isPending = true;
    this.http.get<Pageable<any>>(`${this.selectObj.url}?page=${this.currentPage}&size=5`).subscribe( pageable => {
      this.isPending = false;
      if (this.selectedValue && pageable.content.indexOf(pageable.content
          .filter( next => next[this.selectObj.selectedOpt] === this.selectedValue))) {
        pageable.content.splice(pageable.content.
            indexOf(pageable.content.filter( next => next[this.selectObj.selectedOpt] === this.selectedValue)) - 1, 1);
      }
      this.data = this.data.concat(pageable.content);
      pageable.content.forEach( next => this.selectObj.data.push(next[this.selectObj.selectedOpt]) );
      this.currentPage++;
      this.totalPages = pageable.totalPages;
      const $input = $(document.activeElement).closest('mz-select-container');
      const scrollY = $input.scrollTop();
      setTimeout(_ => {
        $input.find('input').trigger('focus').trigger('click').finish().scrollTop(scrollY);
        setTimeout( _ => $input.scrollTop(scrollY), 50);
      }, 0);
    })
  }

  loadMore(){
    if (this.selectObj.url && !this.isPending && this.currentPage < this.totalPages) {
      console.log('loading...');
      this.loadData();
    }
  }
  // В случае мелти селекта событие отсылаем только после секундной задержки, во избежание многочисленных запрососв на сервер
  multipleChanged() {
    const self = this;
    if (this.timeOut) {
      clearTimeout(this.timeOut);
    }
    this.timeOut = setTimeout(function () {
      self.selectChange.emit();
      self.timeOut = null;
    }, 1000)
  }

}
