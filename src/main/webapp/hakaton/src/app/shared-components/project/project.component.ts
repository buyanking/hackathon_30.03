import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ReportObj} from '../../model/ReportObj';

// Единичный проект
@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {
  @Input() report: ReportObj;
  @Input() editable: boolean;
  @Output() openProjectSetting = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
