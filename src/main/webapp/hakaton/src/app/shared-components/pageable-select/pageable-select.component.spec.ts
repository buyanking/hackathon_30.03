import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageableSelectComponent } from './pageable-select.component';

describe('PageableSelectComponent', () => {
  let component: PageableSelectComponent;
  let fixture: ComponentFixture<PageableSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageableSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageableSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
