import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from '../master/header/header.component';
import {RouterModule} from '@angular/router';
import { SelectComponent } from './select/select.component';
import {
  MzButtonModule, MzCardModule, MzCollapsibleModule, MzIconModule, MzModalModule, MzNavbarModule, MzSelectModule,
  MzSidenavModule, MzSpinnerModule
} from 'ng2-materialize';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import { ProjectComponent } from './project/project.component';
import { LastReportComponent } from './last-report/last-report.component';
import {InfiniteScrollModule} from 'angular2-infinite-scroll';

// Модуль общих для модулей инженера и админа компонентов
@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    BrowserModule,
    MzSelectModule,
    MzCardModule,
    MzCollapsibleModule,
    MzNavbarModule,
    MzSidenavModule,
    MzButtonModule,
    MzIconModule,
    MzModalModule,
    InfiniteScrollModule,
    MzSpinnerModule
  ],
  exports: [
    SelectComponent,
    ProjectComponent,
    LastReportComponent
  ],
  declarations: [ SelectComponent, ProjectComponent, LastReportComponent]
})
export class SharedModelModule { }
