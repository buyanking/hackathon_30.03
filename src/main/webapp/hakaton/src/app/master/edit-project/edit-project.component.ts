import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProjectObj} from '../../model/ProjectObj';
import {ProjectService} from '../../services/project.service';
import {SelectObj} from '../../model/SelectObj';
import {Direction} from '../../model/Direction';
import {UserObj} from '../../model/UserObj';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.css']
})
export class EditProjectComponent implements OnInit {
  @Input() projectId;
  @Input() directions: Direction[];
  @Output() modalClosed = new EventEmitter<ProjectObj>();

  public modalOptions: Materialize.ModalOptions = {
    complete: () => { this.modalClosed.emit(this.project); } // Callback for Modal close
  };

  directionCopy: SelectObj;
  autocompleteUsers: {data: {}};
  query: string;

  choosedUsers: UserObj[];
  pending: boolean = false;

  project: ProjectObj;

  constructor(
    private projectService: ProjectService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.directionCopy = {
      placeholder: 'Направление',
      value: null,
      data: [],
      multiple: false
    };
    this.directions.forEach( next => this.directionCopy.data.push(next.name));
    this.directionCopy.value = 'Направление';
    this.autocompleteUsers = {data: {one: null, two: null}};

    if (this.projectId) {
      this.pending = true;
      this.projectService.getProject(this.projectId).subscribe( next => {
        this.pending = false;
        this.project = next;
        this.directionCopy.value = next.direction.name;
        if (next.users) {
          this.choosedUsers = next.users;
        }
      });
    } else {
      this.project = {
        name: '',
        id: null,
        active: true,
        direction: null
      }
    }

    //Костыль для открытия модального окна
    setTimeout( _ => $('#openModal').trigger('click'), 0);
  }

  saveProject(){
    if (this.project.name && this.project.direction && this.choosedUsers.length > 0) {
      this.projectService.saveProject({id: this.project.id, name: this.project.name,
        direction: this.directions.find(next => this.directionCopy.value === next.name).id})
        .subscribe( project => this.modalClosed.emit(project) );
    }
  }

  loadUsers(){
   this.userService.getUsers(0, this.query).subscribe( page => {
      page.content.forEach(next => this.autocompleteUsers.data[next.fullName] = null);
      const $input = $(document.activeElement);
      const text = $input.val();
      $input.val(null).val(text);
    })
  }
}
