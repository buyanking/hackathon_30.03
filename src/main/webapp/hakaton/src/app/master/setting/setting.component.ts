import { Component, OnInit } from '@angular/core';
import {SelectObj} from '../../model/SelectObj';
import {Setting} from '../../model/Setting';
import {SettingService} from '../../services/setting.service';
import {UserObj} from '../../model/UserObj';
import {UserService} from '../../services/user.service';
import {MzToastService} from 'ng2-materialize';
import {Router} from '@angular/router';

// Раздел настройки
@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.css']
})
export class SettingComponent implements OnInit {
  week: {name: string, id: string}[] = [
    {name: 'Пн', id: 'monday'},
    {name: 'Вт', id: 'tuesday'},
    {name: 'Ср', id: 'wednesday'},
    {name: 'Чт', id: 'thursday'},
    {name: 'Пт', id: 'friday'},
    {name: 'Сб', id: 'saturday'},
    {name: 'Вс', id: 'sunday'}
  ];

  weekSelect: SelectObj = {
    placeholder: 'Выберите день',
    data: [
      'Понедельник',
      'Вторник',
      'Среда',
      'Четверг',
      'Пятница',
      'Суббота',
      'Воскресенье'
      ],
    value: '',
    multiple: false
  };

  timeSelect: SelectObj = {
    placeholder: 'Выберите время',
    data: [
      '00:00'
    ],
    value: '',
    multiple: false
  };

  rolesSelect: SelectObj = {
    placeholder: 'Выберите роль',
    data: [
      'Ответственный',
      'Руководитель',
      'Администратор'
    ],
    value: 'Ответственный',
    multiple: false
  };

  directionSelect: SelectObj = {
    placeholder: 'Направление',
    data: [
      'Разработка',
      'Другое'],
    value: 'Направление',
    multiple: true
  };

  setting: Setting;

  users: UserObj[];
  totalPages: number;
  currentPage: number = 0;
  isPending: boolean = false; // Ожидается ли страница


  constructor(
    private settingService: SettingService,
    private userService: UserService,
    private toastService: MzToastService,
    private router: Router
  ) { }

  ngOnInit() {
    // Проверяем роль пользователя, если не ADMIN то перенаправляем на проекты
    this.userService.getUser().subscribe(user => {
      if (user.role.name !== "ADMIN") {
        this.router.navigate(['/master/projects']);
      }
    });
    // Получаем настройки
    this.settingService.getSetting().subscribe( setting => {
      this.setting = setting;
      this.weekSelect.value = this.weekSelect.data[setting.reportDay];
      this.timeSelect.value = this.timeSelect.data[setting.hour];
    });
    this.users = [];
    this.loadData();
    // Формирования списка со временем 00:00 - 23:00
    for (let i=1; i < 24; i++) {
      if (i < 10) {
        this.timeSelect.data.push('0' + i + ':00');
      } else {
        this.timeSelect.data.push(i + ':00');
      }
    }
  }

  // Загрузка очередной страницы данных
  loadData(){
    this.isPending = true;
    this.userService.getUsers(this.currentPage).subscribe( users => {
      this.isPending = false;
      this.totalPages = users.totalPages;
      this.currentPage++;
      this.users = this.users.concat(users.content);
    });
  }

  dayChange(position: number, value: boolean) {
    this.setting.days = this.setting.days.substr(0, position) + (value ? '1' : '0') + this.setting.days.substr(position + 1);
  }

  save() {
    this.setting.reportDay = this.weekSelect.data.indexOf(this.weekSelect.value);
    this.setting.hour = +this.timeSelect.value.substr(0, 2);
    this.settingService.sendSetting(this.setting).subscribe(_ =>
      this.toastService.show('Настройки сохранены', 3000));
    console.log(this.setting);
  }

  loadMore(){
    if (!this.isPending && this.currentPage < this.totalPages) {
      this.loadData();
    }
  }

}
