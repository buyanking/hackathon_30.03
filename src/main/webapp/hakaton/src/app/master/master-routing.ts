import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ProjectsComponent} from './projects/projects.component';
import {MasterComponent} from './master.component';
import {SettingComponent} from "./setting/setting.component";
import {WeekComponent} from './week/week.component';
import {StatisticComponent} from './statistic/statistic.component';

// модель роутинга для администратора и руководитель направления
const masterRoutes: Routes = [
  { path: 'master', component: MasterComponent, children: [
      {
        path: `projects`, component: ProjectsComponent
      },
      {
        path: 'settings', component: SettingComponent
      },
      {
        path: 'week', component: WeekComponent
      },
      {
        path: 'statistic', component: StatisticComponent
      },
      {
        path: 'statistic/:id', component: StatisticComponent
      },
      {
        path: '', redirectTo: 'home', pathMatch: 'full'
      }
      ]
  }];

@NgModule({
  imports: [
    RouterModule.forChild(masterRoutes)
  ],
  exports: [ RouterModule ]
})
export class MasterRoutingModule {
}
