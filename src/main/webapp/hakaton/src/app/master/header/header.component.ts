import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LoginService} from '../../services/login.service';
import {UserObj} from '../../model/UserObj';
import {UserService} from '../../services/user.service';


// Шапка для администратора и руководителя направления
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Input() user: UserObj;

  constructor(
    private router: Router,
    private loginService: LoginService,
    public userService: UserService
  ) { }

  ngOnInit() {
  }

  exit() {
    this.loginService.logout();
  }

  closeNavBar(){
    $('#sidenav').sideNav('hide');
  }
}
