import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsComponent } from './projects/projects.component';
import {MasterComponent} from './master.component';
import {MasterRoutingModule} from './master-routing';
import {SharedModelModule} from '../shared-components/shared-model.module';
import { SettingComponent } from './setting/setting.component';
import {
  MzButtonModule, MzCardModule, MzCheckboxModule, MzCollapsibleModule, MzDatepickerModule, MzDropdownModule, MzIconMdiModule, MzIconModule,
  MzInputModule,
  MzModalModule,
  MzNavbarModule,
  MzSelectModule, MzSidenavModule, MzSpinnerModule,
  MzTabModule, MzToastModule
} from 'ng2-materialize';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import { WeekComponent } from './week/week.component';
import {ChartsModule} from 'ng2-charts';
import { StatisticComponent } from './statistic/statistic.component';
import { GeneralStatisticComponent } from './general-statistic/general-statistic.component';
import { ProjectStatisticComponent } from './project-statistic/project-statistic.component';
import {HeaderComponent} from './header/header.component';
import {InfiniteScroll, InfiniteScrollModule} from 'angular2-infinite-scroll';
import { EditProjectComponent } from './edit-project/edit-project.component';

// модуль администратора и руководителя направления
@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    MasterRoutingModule,
    SharedModelModule,
    MzButtonModule,
    MzInputModule,
    MzTabModule,
    MzCardModule,
    MzNavbarModule,
    MzIconModule,
    MzIconMdiModule,
    MzInputModule,
    MzSelectModule,
    MzCollapsibleModule,
    ChartsModule,
    MzModalModule,
    MzSidenavModule,
    MzDatepickerModule,
    InfiniteScrollModule,
    MzToastModule,
    MzDropdownModule,
    MzSpinnerModule
  ],
  declarations: [
    MasterComponent, ProjectsComponent, SettingComponent, WeekComponent, StatisticComponent, GeneralStatisticComponent,
    EditProjectComponent,  ProjectStatisticComponent, HeaderComponent
  ]
})
export class MasterModule { }
