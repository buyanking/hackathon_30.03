import { Component, OnInit } from '@angular/core';
import {SelectObj} from '../../model/SelectObj';
import {ReportObj} from '../../model/ReportObj';
import {ReportService} from '../../services/report.service';
import {ProjectObj} from '../../model/ProjectObj';
import {Direct} from 'protractor/built/driverProviders';
import {DirectionService} from '../../services/direction.service';
import {Direction} from '../../model/Direction';

// Раздел проекты
@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  editProject: ProjectObj;
  showModal: boolean = false;

  directionSelect: SelectObj = {
    placeholder: '',
    data: ['Направление'],
    value: 'Направление',
    multiple: false
  };
  directions: Direction[];

  reports: ReportObj[];
  query: string = '';
  active: boolean = true;

  totalPages: number;
  currentPage: number = 0;
  isPending: boolean = false; // Загружается ли следующая страница

  constructor(
    private reportService: ReportService,
    private directionService: DirectionService
  ) { }

  ngOnInit() {
    this.reports = [];
    this.directionService.getDirections().subscribe( directions => {
      this.directions = directions;
      directions.forEach( direction => {
        this.directionSelect.data.push(direction.name);
      })
    });
    this.loadData();
  }

  // Загрузка очередной страницы
  loadData(){
    this.isPending = true;
    this.reportService.getProjectReports(this.currentPage).subscribe( pageable => {
      this.isPending = false;
      this.reports = this.reports.concat(pageable.content);
      this.currentPage++;
      this.totalPages = pageable.totalPages;
    })
  }

  openEditModal(project) {
    this.editProject = project;
    this.showModal = true;
  }

  updateData(project: ProjectObj) {
    this.showModal = false;
  }

  reloadData(){
    console.log('reloadData', this.query, this.directionSelect.value, this.active);
  }

  changeActive(active: boolean){
    this.active = active;
    this.reloadData();
  }

  changeType(){
    this.reloadData();
  }

  loadMore(){
    if (!this.isPending && this.currentPage < this.totalPages) {
      this.loadData();
    }
  }
}
