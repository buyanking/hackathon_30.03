import { Component, OnInit } from '@angular/core';
import {SelectObj} from '../../model/SelectObj';
import {WeekControlService} from '../../services/week-control.service';
import {MzToastService} from 'ng2-materialize';

// Глобальная статистика
@Component({
  selector: 'app-general-statistic',
  templateUrl: './general-statistic.component.html',
  styleUrls: ['./general-statistic.component.css']
})
export class GeneralStatisticComponent implements OnInit {
  reload: boolean = false; // Перезагружается ли график
  lines: boolean[] = [true, true, true, true]; // Массив отвещающий за отрисовку линий true - отрисовывать, false - нет
  private lineChartData:Array<any> = [ // Данные для графика
    {data: [0.5, 3.5]},
    {data: [1, 2, 2, 3, 2, 3, 2, 2], label: ''},
    {data: [2, 3, 3, 2, 3, 2, 1, 1], label: ''},
    {data: [3, 3, 1, 3, 2, 3, 2, 3], label: ''},
    {data: [2, 1, 2, 1, 1, 1, 3, 3], label: ''}
  ];
  public lineChartDataCurrent:Array<any> = this.lineChartData.slice();
  public lineChartLabels:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July']; // Единица измерения оси абсцисс
  public lineChartOptions:any = {
    responsive: true
  };
  private lineChartColors:Array<any> = [ // Цвета линий
    {
      backgroundColor: 'rgba(0, 0, 0, 0)',
      borderColor: 'rgba(0, 0, 0, 0)',
      pointBackgroundColor: 'rgba(0, 0, 0, 0)',
      pointBorderColor: 'rgba(0, 0, 0, 0)'
    },
    {
      backgroundColor: 'rgba(0,0,0,0)',
      borderColor: 'rgb(107, 191, 224)',
      pointBackgroundColor: '#fff',
      pointBorderColor: 'rgb(107, 191, 224)'
    },
    {
      backgroundColor: 'rgba(0,0,0,0)',
      borderColor: 'rgba(147, 30, 189, 0.4)',
      pointBackgroundColor: '#fff',
      pointBorderColor: 'rgba(147, 30, 189, 0.4)'
    },
    {
      backgroundColor: 'rgba(0,0,0,0)',
      borderColor: '#3762D9',
      pointBackgroundColor: '#fff',
      pointBorderColor: '#3762D9'
    },
    {
      backgroundColor: 'rgba(0,0,0,0)',
      borderColor: '#ff5b49',
      pointBackgroundColor: '#fff',
      pointBorderColor: '#ff5b49'
    }
  ];

  public lineChartColorsCurrent: Array<any> = this.lineChartColors.slice();

  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';

  public isClosed: boolean = false; // Фильтр (учитывать ли закрытые проекты)

  options: Pickadate.DateOptions = {
    clear: 'Очистить', // Clear button text
    close: 'ОК',    // Ok button text
    today: 'Сегодня', // Today button text
    formatSubmit: 'dd.mm.yyyy',
    firstDay: 1,
    monthsFull: [ 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' ],
    monthsShort: [ 'Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Ноя', 'Дек' ],
    weekdaysFull: [ 'Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота' ],
    weekdaysShort: [ 'Вос', 'Пон', 'Вт', 'Ср', 'Чет', 'Пят', 'Суб' ],
    weekdaysLetter: [ 'В', 'П', 'В', 'С', 'Ч', 'П', 'С' ],
    onClose: this.dateValidation.bind(this)
  };

  public fromTime: string;
  public byTime: string;

  constructor(
    private weekService: WeekControlService,
    private toastService: MzToastService
  ) { }

  ngOnInit() {
    // Устанавливаем фильтр даты с 01.01.2018 по текущий день
    this.fromTime = '01.01.2018';
    const now = new Date();
    const day = now.getDate() < 10 ? '0' + now.getDate() : now.getDate();
    const month = (now.getMonth() + 1) < 10 ? '0' + (now.getMonth() + 1) : (now.getMonth() + 1);
    this.byTime = day + '.' + month + '.' + now.getFullYear();
    console.log(this.byTime)
  }
  // Инициализация объекта селекта с направлениями
  directionSelect: SelectObj = {
    placeholder: 'Направление',
    data: ['Разработка',
      'Другое'],
    value: null,
    multiple: true
  };

  //Перерисовка графика
  private reDraw() {
    let _lineChartDataCurrent = this.lineChartData.slice(0, 1);
    let _lineChartColorsCurrent = this.lineChartColors.slice(0, 1);
    let newData = [
      {data: [0.5, 3.5]},
      {data: [1, 2, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 1, 2, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 1, 2, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 1, 2, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 1, 2, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3], label: ''},
      {data: [2, 3, 3, 2, 3, 2, 2, 2, 3, 2, 3, 2, 2, 2, 3, 2, 3, 2, 2, 3, 3, 2, 3, 2, 2, 2, 3, 2, 3, 2, 2, 2, 3, 2, 3, 2, 2, 3, 3, 2, 3, 2, 2, 2, 3, 2, 3, 2, 2, 2, 3, 2, 3, 2, 3, 3, 2, 3, 2, 2, 2, 3, 2, 3, 2, 2, 2, 3, 2, 3, 2, 2, 3, 3, 2, 3, 2, 2, 2, 3, 2, 3, 2, 2, 2, 3, 2, 3, 2, 2, 3, 3, 2, 3, 2, 2, 2, 3, 2, 3, 2, 2, 2, 3, 2, 3, 2], label: ''},
      {data: [3, 3, 1, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 3, 3, 1, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 3, 3, 1, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 3, 1, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 3, 3, 1, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 3, 3, 1, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3], label: ''},
      {data: [2, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1], label: ''}
    ];
    const self = this;
    newData.forEach(function (element, index) {
      if(index > 0 && self.lines[index - 1]) {
        _lineChartDataCurrent.push(element);
        _lineChartColorsCurrent.push(self.lineChartColors[index]);
      }
    });
    this.reload = true;
    // Удаляем канвас изображение, чтобы реиницилизировать его
    setTimeout(function () {
      self.reload = false;
      self.lineChartColorsCurrent = _lineChartColorsCurrent;
      self.lineChartDataCurrent = _lineChartDataCurrent;
      self.lineChartLabels = self.weekService.formatGraphData(self.lineChartDataCurrent, self.fromTime).labels;
    }, 0);
    console.log(this.lineChartDataCurrent, this.lineChartColorsCurrent)
  }

  // Изменения видимости линии
  changeLine(value: boolean, index: number) {
    this.lines[index] = value;
    console.log(this.lines);
    this.reDraw();
  }

  // Перезагрузка даты
  reloadData() {
    console.log('reload data:', this.isClosed, this.directionSelect.value, this.fromTime, this.byTime);
  }

  // Изменения фильтра закрытых проектов
  changeClosed(closed: boolean) {
    this.isClosed = closed;
    this.reloadData();
  }

  // Валидация введенной даты
  dateValidation(): void {
    if (this.fromTime) {
      const from = new Date(this.fromTime.split('.').reverse().join('-'));
      if (from.getTime() > Date.now()) {
        this.fromTime = '';
        this.toastService.show('Дата начала не может быть больше текущей даты', 3000);
      }
    }
    if(this.byTime && this.fromTime) {
      const from = new Date(this.fromTime.split('.').reverse().join('-'));
      const by = new Date(this.byTime.split('.').reverse().join('-'));
       if (by.getTime() < from.getTime()) {
        this.byTime = '';
        this.toastService.show('Дата начала не может быть позднее окончания', 3000);
      } else {
         this.reloadData();
       }
    }
  }
}
