import { Component, OnInit } from '@angular/core';
import {SelectObj} from '../../model/SelectObj';
import {ActivatedRoute} from '@angular/router';
import {ReportService} from '../../services/report.service';
import {ReportObj} from '../../model/ReportObj';
import {isPending} from 'q';

// Раздел статичтика
@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.css']
})
export class StatisticComponent implements OnInit {
  projectId: number;

  reports: ReportObj[];

  totalPages: number;
  currentPage: number = 0;
  isPending: boolean = false; // Ожидается ли страница

  ngOnInit(): void {
    // Получаем id проекта из url, если его нету отображаем общую статистику
    this.route.paramMap.subscribe(next => {
      this.projectId = +next.get('id');
      this.reports = [];
      this.loadData();
    });
  }

  constructor(
    private route: ActivatedRoute,
    private reportService: ReportService
  ) {}

  // Загрузка очередной страницы
  loadData(){
    this.isPending = true;
    this.reportService.getProjectReports(this.currentPage).subscribe( pageable => {
      this.isPending = false;
      this.reports = this.reports.concat(pageable.content);
      this.currentPage++;
      this.totalPages = pageable.totalPages;
    })
  }

  loadMore(){
    if (!this.isPending && this.currentPage < this.totalPages) {
      this.loadData();
    }
  }

}
