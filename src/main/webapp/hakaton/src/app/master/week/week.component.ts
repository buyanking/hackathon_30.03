import { Component, OnInit } from '@angular/core';
import {SelectObj} from '../../model/SelectObj';
import {WeekControlService} from '../../services/week-control.service';
import {ProjectComponent} from '../../shared-components/project/project.component';
import {ProjectService} from '../../services/project.service';
import {ReportObj} from '../../model/ReportObj';
import {ReportService} from '../../services/report.service';
import {isPending} from 'q';
import {ReportFilter} from '../../model/ReportFilter';
import {Direction} from '../../model/Direction';
import {DirectionService} from '../../services/direction.service';

// Раздел недели
@Component({
  selector: 'app-week',
  templateUrl: './week.component.html',
  styleUrls: ['./week.component.css']
})
export class WeekComponent implements OnInit {
  reportFilter: ReportFilter;
  period: string = '';

  reports: ReportObj[];
  totalPages: number;
  currentPage: number = 0;

  directions: Direction[];

  isPending: boolean = false; // Ожидается ли подгрузка страницы

  options: Pickadate.DateOptions = {
    clear: 'Очистить', // Clear button text
    close: 'ОК',    // Ok button text
    today: 'Сегодня', // Today button text
    onClose: () => this.formatDate(),
    formatSubmit: 'dd.mm.yyyy',
    firstDay: 1,
    monthsFull: [ 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' ],
    monthsShort: [ 'Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Ноя', 'Дек' ],
    weekdaysFull: [ 'Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота' ],
    weekdaysShort: [ 'Вос', 'Пон', 'Вт', 'Ср', 'Чет', 'Пят', 'Суб' ],
    weekdaysLetter: [ 'В', 'П', 'В', 'С', 'Ч', 'П', 'С' ],
  };

  directionSelect: SelectObj;
  constructor(
    private weekService: WeekControlService,
    private reportService: ReportService,
    private directionService: DirectionService
  ) { }

  ngOnInit() {
    this.reports = [];
    this.reportFilter = {
      isActive: true,
      from: null,
      directionId: null,
      to: null
    };
    this.loadData();

    this.directionService.getDirections().subscribe( directions => {
      this.directions = directions;
      this.directionSelect = {
        placeholder: '',
        data: [ 'Выберите направление' ],
        value: 'Выберите направление',
        multiple: false
      };
      directions.forEach( value => this.directionSelect.data.push(value.name));
    })
  }

  // Загрузка очередной страницы
  loadData(){
    this.isPending = true;
    this.reportService.getWeekReports(this.currentPage, this.reportFilter).subscribe( pageable => {
      this.isPending = false;
      this.reports = this.reports.concat(pageable.content);
      this.currentPage++;
      this.totalPages = pageable.totalPages;
    })
  }

  reloadData() {
    this.reports = [];
    this.currentPage = 0;
    this.loadData();
  }

  changeActive(active: boolean) {
    this.reportFilter.isActive = active;
    this.reloadData();
  }

  changeDirection() {
    const findedDirection = this.directions.find( next => next.name === this.directionSelect.value);
    this.reportFilter.directionId = findedDirection ? findedDirection.id : null;
    if (this.reportFilter.directionId == null) {
      this.reportFilter.directionId = null;
    }
    this.reloadData();
  }

  // После выбора даты отображаем неделю, в которой находится выбранная дата
  formatDate() {
    //this.period = event.target.value;
    if (this.period) {
      this.period = this.weekService.formatDate(this.period.substr(0, 10));
      this.options.formatSubmit = this.period;
      setTimeout(() => $('#datepicker').val(this.period), 0);
    }
    this.reportFilter.from = this.period.split(' — ')[0].trim();
    this.reportFilter.to = this.period.split(' — ')[1].trim();
    this.reloadData();
  }

  loadMore(){
    if (!this.isPending && this.currentPage < this.totalPages) {
      this.loadData();
    }
  }
}
