import { Component } from '@angular/core';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';

// Корневой компонент пользователей администратор и руководитель направления
// Служит для навигации по дочерним элементам и проверки роли пользователя
@Component({
  template: '<app-header *ngIf="userService.user" [user]="userService.user"></app-header><router-outlet></router-outlet>'
})
export class MasterComponent {
  constructor (
    public userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.userService.getUser().subscribe( user => {
      if (!~[1, 2].indexOf(user.role.id) ){
        this.router.navigate(['/']);
      }
    });
  }
}
