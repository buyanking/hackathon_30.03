// Pageable объект
export class Pageable<T> {
  content: T[];
  first: boolean;
  last: boolean;
  number: number;
  numberOfElements: number;
  totalPages: number;
  size: number;
}
