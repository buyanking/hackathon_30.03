// Объект пользователя
export class UserObj{
  id: number;
  role: {
    id: number,
    name: string
  };
  name: string;
  surname: string;
  patronymic: string;
  fullName: string;
  email: string;
  phone: string;
  photo: string;
}
