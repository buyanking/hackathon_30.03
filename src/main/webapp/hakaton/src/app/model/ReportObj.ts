import {ProjectObj} from './ProjectObj';
import {UserObj} from './UserObj';
import {LevelEnun} from './LevelEnum';

// Объект отчета
export class ReportObj{
  id: number;
  project: ProjectObj;
  user: UserObj;
  comment: string;
  createDate: string;
  teamMotivation: LevelEnun;
  teamCompetence: LevelEnun;
  successOfTheProject: LevelEnun;
  customerSatisfaction: LevelEnun;
}
