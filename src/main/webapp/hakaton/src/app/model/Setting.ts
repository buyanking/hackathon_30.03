// Объект настроек
export class Setting{
  id: number;
  days: string;
  reportDay: number;
  hour: number;
  minute: number;
  attentionText: string;
}
