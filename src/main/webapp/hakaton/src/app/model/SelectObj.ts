// Объект для компонента - селекта
export class SelectObj{
  placeholder: string;
  value: string;
  data: string[];
  multiple: boolean;
  url?: string;
  selectedOpt?: string;
}
