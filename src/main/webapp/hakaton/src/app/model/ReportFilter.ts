export class ReportFilter{
  from: string;
  to: string | null;
  directionId: number;
  isActive: boolean;
}
