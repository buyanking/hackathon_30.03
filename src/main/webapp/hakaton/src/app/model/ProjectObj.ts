// Объект проекта
import {UserObj} from './UserObj';
import {Direction} from './Direction';

export class ProjectObj {
  id: number;
  name: string;
  direction: Direction;
  hasReports?: string;
  active?: boolean;
  users?: UserObj[];
}
