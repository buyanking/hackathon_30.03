import { Component, OnInit } from '@angular/core';
import {LoginService} from '../services/login.service';
import {Router} from '@angular/router';
import {UserService} from '../services/user.service';


// Форма входа
@Component({
  selector: 'app-welcome-screen',
  templateUrl: './welcome-screen.component.html',
  styleUrls: ['./welcome-screen.component.css']
})
export class WelcomeScreenComponent implements OnInit {
  username: string;
  password: string;
  error: string;

  // Валидация и вход
  login (): void {
    if (this.username && this.password) {
      this.loginService.login(this.username, this.password).subscribe(next => {
          this.ngOnInit();
        },
        e => {
          if (e.status === 401) {
            this.error = 'Не выерный логин или пароль';
          } else {
            this.error = 'Ошибка сервера';
          }
        });
    } else if (!this.username && !this.password) {
      this.error = 'Введите логин и пароль!';
    } else if (!this.username) {
      this.error = 'Введите логин!';
    } else {
      this.error = 'Введите пароль!';
    }
  }

  constructor(
    private loginService: LoginService,
    private userInfoService: UserService,
    private router: Router
  ) { }

  // Проверяем не авторизовался ли уже пользователь и редиректим если да
  ngOnInit() {
    this.userInfoService.getUser().subscribe( user => {
      switch (user.role.id) {
        case 1:
        case 2:
          this.router.navigate(['master/projects']);
          break;
        case 3:
          this.router.navigate(['engineer']);
          break;
        default:
          this.router.navigate(['/']);
          break;
      }
    });
  }

  // При нажатии enter в поле логин ереходим в поле пароль
  nextField(event, field) {
    if(event.keyCode == 13) {
      field.focus();
    }
  }

  // При нажатии enter в поле пароль отправляем форму
  sendOnEnter(event) {
    if(event.keyCode == 13) {
      this.login();
    }
  }

}
