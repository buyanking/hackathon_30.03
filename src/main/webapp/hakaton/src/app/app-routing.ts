import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {WelcomeScreenComponent} from './welcome-screen/welcome-screen.component';
import {MasterComponent} from './master/master.component';
import {EngineerComponent} from './engineer/engineer.component';

// Схема корневого роутинга
const appRoutes: Routes = [
  { path: 'master', component: MasterComponent },
  { path: 'engineer', component: EngineerComponent },
  { path: 'login', component: WelcomeScreenComponent },
  { path: ``, redirectTo: '/login', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {useHash: true})
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModel {
}
