import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';
import {catchError, tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {UserObj} from '../model/UserObj';
import {ProjectObj} from '../model/ProjectObj';
import {Pageable} from '../model/Pageable';

// Сервис для отправки - получения проектов
@Injectable()
export class ProjectService {
  public spinner = false;

  getProjects(): Observable<Pageable<ProjectObj>> {
    this.spinner = true;
    return this.http.get<Pageable<ProjectObj>>(`api/project`).pipe(
      tap(_ => this.log(`Get user projects`)),
      catchError(this.handleError<Pageable<ProjectObj>>('cant catch projects'))
    );
  }

  getProject(id: number): Observable<ProjectObj> {
    this.spinner = true;
    return this.http.get<Pageable<ProjectObj>>(`api/project`).pipe(
      tap(_ => this.log(`Get user projects`)),
      catchError(this.handleError<Pageable<ProjectObj>>('cant catch projects'))
    ).map(next => next.content.find( el => el.id === id));
  }

  getUserProjects(): Observable<ProjectObj[]> {
    this.spinner = true;
    return this.http.get<ProjectObj[]>(`api/myProjects`).pipe(
      tap(_ => this.log(`Get user projects`)),
      catchError(this.handleError<ProjectObj[]>('cant catch projects'))
    );
  }

  saveProject(project: {id: null|number, name: string, direction: number, users?: number[]}): Observable<ProjectObj> {
    return this.http.post<ProjectObj>(`api/project`, project).pipe(
      tap(_ => this.log(`Save project ${project.name}`)),
      catchError(this.handleError<ProjectObj>('cant save project'))
    )
  }

  private log(message: string) {
    this.spinner = false;
    console.log(message);
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      if (error.status === 401) {
        this.router.navigate(['/login']);
      }
      //console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }


}
