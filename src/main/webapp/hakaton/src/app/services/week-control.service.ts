import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {ReportObj} from '../model/ReportObj';
import {el} from '@angular/platform-browser/testing/src/browser_util';

// Сервис для форматирования даты и данных для статистики
@Injectable()
export class WeekControlService {
  private monthsName: string[] = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'
  ];

  // метод определяет находится ли дата в пределах текущей недели
  isSended (createDateStr: string) {
    let createDate;
    if (~createDateStr.indexOf('.')) {
      createDate = new Date(createDateStr.split('.').reverse().join('-'));
    } else {
      createDate = new Date(createDateStr);
    }
    const date = new Date();
    const startWeek = new Date(date.getTime() - date.getDay() * 1000 * 60 * 60 * 24);
    startWeek.setHours(0);
    startWeek.setMinutes(0);
    startWeek.setSeconds(0);
    startWeek.setMilliseconds(0);
    return createDate > startWeek;
  }

  // Форматирование даты (из даты создания в промежуток недели, в которой находится дата)
  formatDate (createDate: string) {
    let date;
    if (~createDate.indexOf('.')) {
      date = new Date(createDate.split('.').reverse().join('-'));
    } else {
      date = new Date(createDate);
    }
    const startWeek = new Date(date.getTime() - date.getDay() * 1000 * 60 * 60 * 24);
    const endWeek = new Date(date.getTime() + (7 - date.getDay()) * 1000 * 60 * 60 * 24);
    return this.addNull(startWeek.getDate() + 1) + '.' + this.addNull((startWeek.getMonth() + 1)) + '.' + startWeek.getFullYear() + ' — '
     + this.addNull(endWeek.getDate()) + '.' + this.addNull(endWeek.getMonth() + 1) + '.' + endWeek.getFullYear();
  }

  // аналогично предыдущей, только формат (1 января - 23 марта)
  monthWordFormat (createDate: string) {
    let date;
    if (~createDate.indexOf('.')) {
      date = new Date(createDate.split('.').reverse().join('-'));
    } else {
      date = new Date(createDate);
    }
    const startWeek = new Date(date.getTime() - date.getDay() * 1000 * 60 * 60 * 24);
    const endWeek = new Date(date.getTime() + (7 - date.getDay()) * 1000 * 60 * 60 * 24);
    return this.addNull(startWeek.getDate() + 1) + ' ' + this.getMonth(startWeek.getMonth()) + ' — '
      + this.addNull(endWeek.getDate()) + ' ' + this.getMonth(endWeek.getMonth());
  }

  private getMonth(month: number) {
    switch(month){
      case 2:
      case 7:
        return this.monthsName[month].slice(0, this.monthsName[month].length - 1).toLowerCase() + 'а';
      default:
        return this.monthsName[month].slice(0, this.monthsName[month].length - 1).toLowerCase() + 'я';
    }
  }

  // форматирование данных для статичтики
  formatGraphData( data: {data: number[]}[], fromDate: string ) : {data : {data: {data: number[], label: string}}[], labels: string[] } {
    if (data.length < 2) {
      return null;
    }
    let dateFrom;
    if (~fromDate.indexOf('.')) {
      dateFrom = new Date(fromDate.split('.').reverse().join('-'));
    } else {
      dateFrom = new Date(fromDate);
    }
    const self = this;
    let monthIndex = dateFrom.getMonth();
    let finalData, finalMonths;
    switch (true) {
      case data[1].data.length < 6:
        finalData = data.slice();
        finalMonths = [];
        data[1].data.forEach(function (element, index) {
          if (index === 0) {
            finalMonths.push(self.monthsName[monthIndex]);
          } else if (index % 4 === 0) {
            finalMonths.push(self.monthsName[monthIndex]);
            monthIndex++;
          } else {
            finalMonths.push(index % 4 + '/4 ' + self.monthsName[monthIndex]);
          }
        });
        return {data: finalData, labels: finalMonths};
      case data[1].data.length < 12:
        finalData = data.slice(0, 1);
        finalMonths = [];
        data.forEach(function (element, index) {
          if (index > 0) {
            finalData[index] = [];
            // Берем среднее арифметическое по каждым 2 значениям массива
            element.data.forEach(function (el, i) {
              if (i + 1 % 2 === 0) {
                finalData[index].push({data: (element.data[i-1] + element.data[i]) / 2, label: ''});
              }
              if (index === 1) {
                if (i === 0) {
                  finalMonths.push(self.monthsName[monthIndex]);
                } else if (i % 2 === 0) {
                  finalMonths.push(self.monthsName[monthIndex]);
                  monthIndex++;
                } else {
                  finalMonths.push('1/2 ' + self.monthsName[monthIndex]);
                }
              }
            })
          }
        });
        return {data: finalData, labels: finalMonths};
      case data[1].data.length < 48:
        finalData = data.slice(0, 1);
        finalMonths = [];
        data.forEach(function (element, index) {
          if (index > 0) {
            finalData[index] = [];
            // Берем среднее арифметическое по каждым 4 значениям массива
            element.data.forEach(function (el, i) {
              if (i + 1 % 4 === 0) {
                finalData[index].push({data: (element.data[i-3] + element.data[i-2] + element.data[i-1] + element.data[i]) / 4, label: ''});
              }
              if (index === 1) {
                if (i === 0) {
                  finalMonths.push(self.monthsName[monthIndex]);
                  monthIndex++;
                } else if (i % 4 === 0) {
                  finalMonths.push(self.monthsName[monthIndex]);
                  monthIndex++;
                }
              }
            })
          }
        });
        return {data: finalData, labels: finalMonths};
      case data[1].data.length < 48 * 6:
        finalData = data.slice(0, 1);
        finalMonths = [];
        let year = dateFrom.getFullYear();
        data.forEach(function (element, index) {
          if (index > 0) {
            finalData[index] = [];
            // Берем среднее арифметическое по каждым 4 значениям массива
            element.data.forEach(function (el, i) {
              if (i + 1 % 24 === 0) {
                let sum = 0;
                for (let t=0; t < 24; i++) {
                  sum += element[i-t];
                }
                finalData[index].push({data: sum / 24, label: ''});
              }
              if (index === 1) {
                if (i === 0) {
                  finalMonths.push(self.monthsName[monthIndex % 12]);
                  monthIndex += 6;
                } else if (i % 24 === 0) {
                  if (i % 48 === 0) {
                    finalMonths.push(year + Math.round(monthIndex / 12));
                  } else {
                    finalMonths.push(self.monthsName[monthIndex % 12]);
                  }
                  monthIndex += 6;
                }
              }
            })
          }
        });
        return {data: finalData, labels: finalMonths};
    }
  }

  // Добавление нуля перед однозначным числом
  addNull(day: number) : string {
    if (day < 10) {
      return '0' + day;
    }
    else return day.toString();
  }

  constructor(
  ) { }


}
