import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';
import {tap} from 'rxjs/operators';
import {UserService} from './user.service';
import {Router} from '@angular/router';

// Сервис для входа - выхода
@Injectable()
export class LoginService {
  public spinner = false;

  login(username: string, password: string): Observable<string> {
    this.spinner = true;
    return this.http.post<string>(`api/authentication?username=${username}&password=${password}`, null).pipe(
      tap(_ => this.log(`Login as ${username}`)),
    );
  }

  logout(): void {
    this.spinner = true;
    this.http.post<string>(`api/logout`, null).pipe(
      tap(_ => this.log(`Logout`)),
    ).subscribe(next => {
      this.userService.user = null;
      this.router.navigate(['/']);
    });
  }

  private log(message: string) {
    this.spinner = false;
    console.log(message);
  }

  constructor(
    private http: HttpClient,
    private userService: UserService,
    private router: Router
  ) { }


}
