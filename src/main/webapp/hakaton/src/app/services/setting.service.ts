import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';
import {catchError, tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {UserObj} from '../model/UserObj';
import {ProjectObj} from '../model/ProjectObj';
import {ReportObj} from '../model/ReportObj';
import {Pageable} from '../model/Pageable';
import {Setting} from '../model/Setting';

// Сервис для отправки - получения настроек
@Injectable()
export class SettingService {
  public spinner = false;

  getSetting(): Observable<Setting> {
    this.spinner = true;
    return this.http.get<Setting>(`api/setting`).pipe(
      tap(_ => this.log(`Get settings`)),
      catchError(this.handleError<Setting>('cant catch settings'))
    );
  }

  sendSetting(data): Observable<Setting> {
    this.spinner = true;
    return this.http.post<Setting>(`api/setting`, data).pipe(
      tap(_ => this.log(`Send setting`)),
      catchError(this.handleError<Setting>('cant send setting'))
    )
  }

  private log(message: string) {
    this.spinner = false;
    console.log(message);
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      if (error.status === 401) {
        this.router.navigate(['/login']);
      }
      //console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }


}
