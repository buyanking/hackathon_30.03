import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';
import {catchError, tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {ReportObj} from '../model/ReportObj';
import {Pageable} from '../model/Pageable';
import {ReportFilter} from '../model/ReportFilter';

// Сервис для отправки - получения отчетов
@Injectable()
export class ReportService {
  public spinner = false;

  getProjectReports(page: number): Observable<Pageable<ReportObj>> {
    this.spinner = true;
    return this.http.get<Pageable<ReportObj>>(`api/user/report?page=${page}&size=5`).pipe(
      tap(_ => this.log(`Get project reports`)),
      catchError(this.handleError<Pageable<ReportObj>>('cant catch project reports'))
    );
  }

  getWeekReports(page: number, filter: ReportFilter): Observable<Pageable<ReportObj>> {
    this.spinner = true;
    return this.http.get<Pageable<ReportObj>>(`api/user/report/week?page=${page}&size=5`
      + `&isActive=${filter.isActive ? '' : filter.isActive}`
      + `&from=${filter.from ? filter.from : ''}`
      + `&to=${filter.to ? filter.to : ''}`
      + `&directionId=${filter.directionId ? filter.directionId : ''}`).pipe(
      tap(_ => this.log(`Get project reports`)),
      catchError(this.handleError<Pageable<ReportObj>>('cant catch project reports'))
    );
  }

  sendReport(data): Observable<ReportObj> {
    this.spinner = true;
    return this.http.post<ReportObj>(`api/report`, data).pipe(
      tap(_ => this.log(`Send report`)),
      catchError(this.handleError<ReportObj>('cant send project report'))
    );
  }

  private log(message: string) {
    this.spinner = false;
    console.log(message);
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      if (error.status === 401) {
        this.router.navigate(['/login']);
      }
      //console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }


}
