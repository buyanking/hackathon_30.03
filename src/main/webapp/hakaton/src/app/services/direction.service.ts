import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';
import {catchError, tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {ReportObj} from '../model/ReportObj';
import {Pageable} from '../model/Pageable';
import {Direction} from '../model/Direction';

// Сервис для отправки - получения отчетов
@Injectable()
export class DirectionService {
  public spinner = false;

  getDirections(): Observable<Direction[]> {
    this.spinner = true;
    return this.http.get<Direction[]>(`api/direction`).pipe(
      tap(_ => this.log(`Get directions`)),
      catchError(this.handleError<Direction[]>('cant catch directions'))
    );
  }

  private log(message: string) {
    this.spinner = false;
    console.log(message);
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      if (error.status === 401) {
        this.router.navigate(['/login']);
      }
      //console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }


}
