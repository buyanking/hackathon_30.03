import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';
import {catchError, tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {UserObj} from '../model/UserObj';
import {Pageable} from '../model/Pageable';
import {observable} from 'rxjs/symbol/observable';
import {FromObservable} from 'rxjs/observable/FromObservable';
import {ReportObj} from '../model/ReportObj';

// Сервис для работы с пользователями
@Injectable()
export class UserService {
  public spinner = false;
  public user: UserObj;

  private getUserInfo(): Observable<UserObj> {
    this.spinner = true;
    return this.http.get<UserObj>(`api/account`).pipe(
      tap(user => {
        this.log(`Get user ${user.fullName} info`);
        this.user = user;
      }),
      catchError(this.handleError<UserObj>('cant catch user info'))
    );
  }

  getUser(): Observable<UserObj> {
    if (this.user) {
      return FromObservable.create( [this.user]) //observable.create(this.user);
    } else {
      return this.getUserInfo();
    }
  }
  getUsers(page: number, query?: string): Observable<Pageable<UserObj>> {
    this.spinner = true;
    return this.http.get<Pageable<UserObj>>(`api/users?page=${page}` + (query ? `&q=${query}` : ``)).pipe(
      tap(user => this.log(`Get users info`)),
      catchError(this.handleError<Pageable<UserObj>>('cant catch users info'))
    );
  }

  private log(message: string) {
    this.spinner = false;
    console.log(message);
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      if (error.status === 401) {
        this.router.navigate(['/login']);
      }
      //console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }


}
