import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import 'materialize-css';

import { AppComponent } from './app.component';
import {SharedModelModule} from './shared-components/shared-model.module';
import {AppRoutingModel} from './app-routing';
import {MasterModule} from './master/master.module';
import {EngineerModule} from './engineer/engineer.module';
import { WelcomeScreenComponent } from './welcome-screen/welcome-screen.component';
import {LoginService} from './services/login.service';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {MzButtonModule, MzCollapsibleModule, MzInjectionModule, MzInputModule, MzTabModule} from 'ng2-materialize';
import {UserService} from './services/user.service';
import {ProjectService} from './services/project.service';
import {ReportService} from './services/report.service';
import {WeekControlService} from './services/week-control.service';
import {SettingService} from './services/setting.service';
import {DirectionService} from './services/direction.service';

// Корневой модуль
@NgModule({
  declarations: [
    AppComponent,
    WelcomeScreenComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    SharedModelModule,
    MasterModule,
    EngineerModule,
    AppRoutingModel,
    MzInjectionModule,
    MzButtonModule,
    MzInputModule,
    MzCollapsibleModule,
    MzTabModule
  ],
  providers: [ LoginService, UserService, ProjectService, ReportService, WeekControlService, SettingService, DirectionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
