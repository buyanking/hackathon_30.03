import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SelectObj} from '../../model/SelectObj';
import {LoginService} from '../../services/login.service';
import {UserService} from '../../services/user.service';
import {ProjectService} from '../../services/project.service';
import {ProjectObj} from '../../model/ProjectObj';
import {send} from 'q';

// Шапка для инженера
@Component({
  selector: 'app-header-engineer',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  projectsSelect: SelectObj;
  projects: ProjectObj[];
  leftProjects: string;
  @Input() currentProject: ProjectObj;
  @Output() curretnProjectChange = new EventEmitter(); // Событие изменения проекта


  constructor(
    private loginService: LoginService,
    public userService: UserService,
    private projectService: ProjectService
  ) { }

  ngOnInit() {
    // получаем список проектов пользователя и заполняем объект селлекта
    this.projectService.getUserProjects().subscribe(projects => {
      this.projects = projects;
      this.projectsSelect = {
        placeholder: 'Выберите проект',
        data : [],
        value: projects[0] ? projects[0].name : null,
        multiple: false
      };
      let sended = 0;
      projects.forEach(next => {
        this.projectsSelect.data.push(next.name);
        if (next.hasReports) {
          sended ++;
        }
      });
      if (sended === projects.length) {
        this.leftProjects = 'Все отчеты отправлены';
      } else {
        this.leftProjects = 'Осталось отправить ' + (projects.length - sended) + ' из ' + projects.length;
      }
      this.currentProject = projects[0];
      this.changeProject(this.projectsSelect);
    });

  }

  // при изменении проекта находим его id, закрываем sidenav (если он был открыт)
  // и отправляем события изменения проекта родителю
  changeProject(projects: SelectObj) {
    this.projectsSelect = projects;
    this.currentProject = this.projects[this.projectsSelect.data.indexOf(this.projectsSelect.value)];
    this.closeNavBar();
    this.curretnProjectChange.emit({id: this.currentProject.id, name: this.projectsSelect.value});
  }

  closeNavBar() {
    $('#sidenav').sideNav('hide');
  }

  exit() {
    this.loginService.logout();
  }
}
