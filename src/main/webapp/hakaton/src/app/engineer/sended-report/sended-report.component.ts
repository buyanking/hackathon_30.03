import {Component, Input, OnInit} from '@angular/core';
import {ReportObj} from '../../model/ReportObj';
import {WeekControlService} from '../../services/week-control.service';


// Компонент - отправленный отчет (за текущую неделю)
@Component({
  selector: 'app-sended-report',
  templateUrl: './sended-report.component.html',
  styleUrls: ['./sended-report.component.css']
})
export class SendedReportComponent implements OnInit {
  @Input() report: ReportObj;

  constructor(
    public weekService: WeekControlService
  ) { }

  ngOnInit() {
  }

}
