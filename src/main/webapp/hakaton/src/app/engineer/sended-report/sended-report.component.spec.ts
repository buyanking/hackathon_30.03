import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendedReportComponent } from './sended-report.component';

describe('SendedReportComponent', () => {
  let component: SendedReportComponent;
  let fixture: ComponentFixture<SendedReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendedReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendedReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
