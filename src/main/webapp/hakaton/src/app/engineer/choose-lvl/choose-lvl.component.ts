import {Component, Input, OnInit, Renderer} from '@angular/core';

// Компонент выполняет роль плашек выбора уровней удовлетворенности
@Component({
  selector: 'app-choose-lvl',
  templateUrl: './choose-lvl.component.html',
  styleUrls: ['./choose-lvl.component.css']
})
export class ChooseLvlComponent implements OnInit {
  @Input() titles: {title: string, color: string}[];
  color: string[] = [];

  constructor(
    private renderer: Renderer
  ) { }

  ngOnInit() {

  }

  // Выбор цвета оценки и свертывание вариантов
  closeCollapsible(event, color, i) {
    var target = event.target || event.srcElement || event.currentTarget;
    this.color[i] = color;
    $(target).closest('.collapsible').find('.active').removeClass('active');
    $(target).closest('.collapsible').find('.collapsible-body').css('display', '');
    this.titles[i].color = color;
  }
}
