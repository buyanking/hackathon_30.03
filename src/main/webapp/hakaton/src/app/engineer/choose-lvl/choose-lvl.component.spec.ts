import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseLvlComponent } from './choose-lvl.component';

describe('ChooseLvlComponent', () => {
  let component: ChooseLvlComponent;
  let fixture: ComponentFixture<ChooseLvlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseLvlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseLvlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
