import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EngineerComponent} from './engineer.component';
import {RouterModule} from '@angular/router';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {EngineerRouting} from './engineer-routing';
import { EngineerHomeComponent } from './engineer-home/engineer-home.component';
import { HeaderComponent } from './header/header.component';
import {
  MzBadgeModule, MzButtonModule, MzChipModule, MzCollapsibleModule, MzDropdownModule, MzIconMdiModule, MzIconModule, MzModalModule,
  MzSidenavModule, MzToastModule
} from 'ng2-materialize';
import {SharedModelModule} from '../shared-components/shared-model.module';
import { ChooseLvlComponent } from './choose-lvl/choose-lvl.component';
import { AddReportComponent } from './add-report/add-report.component';
import { SendedReportComponent } from './sended-report/sended-report.component';

// Модуль пользователя инженер
@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    EngineerRouting,
    MzCollapsibleModule,
    SharedModelModule,
    MzSidenavModule,
    MzIconMdiModule,
    MzIconModule,
    MzButtonModule,
    MzChipModule,
    MzBadgeModule,
    MzModalModule,
    MzDropdownModule,
    MzToastModule
  ],
  declarations: [EngineerComponent, EngineerHomeComponent, HeaderComponent, ChooseLvlComponent, AddReportComponent, SendedReportComponent]
})
export class EngineerModule { }
