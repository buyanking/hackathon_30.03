import {Component, OnInit} from '@angular/core';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';

// Обертка для инженера, выполняет роль роутинга и проверки роли пользователя
@Component({
  template: '<router-outlet *ngIf="isChecked"></router-outlet>'
})
export class EngineerComponent implements OnInit{
  isChecked = false;

  constructor (
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    // Проверяем роль
    this.userService.getUser().subscribe( user => {
      if (user.role.id !== 3) {
        this.router.navigate(['/']);
      }
      this.isChecked = true;
    });
  }

}
