import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProjectObj} from '../../model/ProjectObj';
import {WeekControlService} from '../../services/week-control.service';
import {LevelEnun} from '../../model/LevelEnum';
import {ReportService} from '../../services/report.service';
import {MzToastService} from 'ng2-materialize';
import {ReportObj} from '../../model/ReportObj';

// компонент создания отчета
@Component({
  selector: 'app-add-report',
  templateUrl: './add-report.component.html',
  styleUrls: ['./add-report.component.css']
})
export class AddReportComponent implements OnInit {
  @Input() project: ProjectObj;
  @Output() reportSended  = new EventEmitter();
  currentWeek: string;
  lastDay: string;

  criteria = [{title: 'Уровень мотивации команды', color: ''},
    {title: 'Уровень удовлетворенности заказчика', color: ''},
    {title: 'Уровень компетенций проектной команды', color: ''},
    {title:'Уровень технического успеха проекта', color: ''}];

  comment: string;

  constructor(
    private weekService: WeekControlService,
    private reportService: ReportService,
    private toastService: MzToastService
  ) { }

  ngOnInit() {
    const now = new Date();
    this.currentWeek = this.weekService.monthWordFormat(now.toString());
    this.lastDay = this.currentWeek.split('—')[1];
  }

  sendData() {
    let isCorrect = true;
    const data = {
      projectId: this.project.id,
      comment: this.comment,
      teamMotivation: this.criteria[0].color.toUpperCase(),
      customerSatisfaction: this.criteria[1].color.toUpperCase(),
      teamCompetence: this.criteria[2].color.toUpperCase(),
      successOfTheProject: this.criteria[3].color.toUpperCase()
    };
    if (!data.teamMotivation) {
      isCorrect = false;
      this.toastService.show('Выберите уровень мотивации команды', 3000);
    }
    if (!data.customerSatisfaction) {
      isCorrect = false;
      this.toastService.show('Выберите уровень удовлетворенности заказчика', 3000);
    }
    if (!data.teamCompetence) {
      isCorrect = false;
      this.toastService.show('Выберите уровень компетенций проектной команды', 3000);
    }
    if (!data.successOfTheProject) {
      isCorrect = false;
      this.toastService.show('Выберите уровень технического успеха команды', 3000);
    }
    if (isCorrect) {
      this.reportService.sendReport(data).subscribe(next => {
          this.reportSended.emit(next);
        }
      );
    }
  }

}
