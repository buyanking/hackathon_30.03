import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {EngineerComponent} from './engineer.component';
import {EngineerHomeComponent} from './engineer-home/engineer-home.component';

// Модель роутинга для инженера
const engineerRoutes: Routes = [
  { path: 'engineer', component: EngineerComponent, children: [
      {
        path: ``, component: EngineerHomeComponent
      }
    ]
  }];

@NgModule({
  imports: [
    RouterModule.forChild(engineerRoutes)
  ],
  exports: [ RouterModule ]
})
export class EngineerRouting {
}
