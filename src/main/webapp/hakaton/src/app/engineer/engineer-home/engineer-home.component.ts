import { Component, OnInit } from '@angular/core';
import {ProjectService} from '../../services/project.service';
import {ReportObj} from '../../model/ReportObj';
import {ReportService} from '../../services/report.service';
import {WeekControlService} from '../../services/week-control.service';
import {ProjectObj} from '../../model/ProjectObj';
import {Pageable} from '../../model/Pageable';

// Корневой компонент инженера
@Component({
  selector: 'app-engineer-home',
  templateUrl: './engineer-home.component.html',
  styleUrls: ['./engineer-home.component.css']
})
export class EngineerHomeComponent implements OnInit {
  currentPorject: ProjectObj;
  currentProjectReports: ReportObj[];
  isSended: boolean; // отправлен ли отчет за эту неделю
  totalPages: number;
  currentPage: number = 0;
  isPending: boolean = false; // ожидается ли новая страница с сервера

  constructor(
    private projectService: ProjectService,
    private reportService: ReportService,
    private weekService: WeekControlService
  ) { }

  ngOnInit() {
  }

  changeProject(project: ProjectObj) {
    this.currentPorject = project;
    this.currentPage = 0;
    this.currentProjectReports = [];
    this.loadData();

  }

  private loadData() {
    this.reportService.getProjectReports(this.currentPage).subscribe(reports => {
      this.currentPage++;
      this.isPending = false;
      this.totalPages = reports.totalPages;
      // Добавляем новую страницу к текущим
      this.currentProjectReports = this.currentProjectReports.concat(reports.content);
      // Проверяем есть ли отчет за текущую неделю если это первая страница
      if (this.currentPage === 1 && reports.content.length > 0) {
        this.isSended = this.weekService.isSended(reports.content[0].createDate);
      } else if (this.currentPage === 1){
        this.isSended = null;
      }
    });
  }

  reportSended(report): void {
    this.isSended = true;
    this.currentProjectReports.unshift(report);
  }

  loadMore(){
    if (this.currentPage < this.totalPages && !this.isPending) {
      this.isPending = true;
      this.loadData();
    }
  }


}
