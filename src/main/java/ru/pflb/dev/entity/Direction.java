package ru.pflb.dev.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;

/**
 * Created by Babushkin Vitalii on 31.03.2018.
 */

@Entity(name = "DIRECTION")
@Setter
@Getter
@AllArgsConstructor
@Immutable
public class Direction extends Dictionary{
    
}
