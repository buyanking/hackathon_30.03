package ru.pflb.dev.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.pflb.dev.service.UserService;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Set;

@Entity(name = "PROJECT")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Project {
    @Id
    @SequenceGenerator(
            name = "project_sequence",
            sequenceName = "project_sequence"
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "project_sequence")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "DATE_START")
    private LocalDate startDate;

    @Column(name = "DATE_END")
    private LocalDate endDate;

    @OneToOne
    @JoinColumn(name = "direction_id")
    private Direction direction;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "project")
    @JsonBackReference
    private Set<ProjectChronos> projectChronos;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "USER_PROJECT",
            joinColumns = {@JoinColumn(name = "PROJECT_ID", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "USER_ID", referencedColumnName = "id")}
    )
    private Set<User> users;
}
