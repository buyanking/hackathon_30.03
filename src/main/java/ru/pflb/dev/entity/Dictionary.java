package ru.pflb.dev.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

/**
 * Created by Babushkin Vitalii on 31.03.2018.
 */
@Getter
@Setter
@MappedSuperclass
public class Dictionary implements Serializable{
    @Id
    protected Long id;

    @Column
    protected String name;
}
