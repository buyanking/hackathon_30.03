package ru.pflb.dev.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * Created by Babushkin Vitalii on 31.03.2018.
 */
@Entity(name = "REPORT")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Report implements Serializable{
    @Id
    @SequenceGenerator(
            name = "report_sequence",
            sequenceName = "report_sequence"
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "report_sequence")
    private Long id;

    @OneToOne
    @JoinColumn(name ="USER_ID")
    private User user;

    @OneToOne
    @JoinColumn(name ="PROJECT_ID")
    private Project project;

    @Column(name = "DATE_CREATE")
    private LocalDate createDate;

    @Column(name = "TEXT")
    private String comment;

    @Column(name = "TEAM_MOTIVATION", nullable = false)
    @Enumerated(EnumType.STRING)
    private Rating teamMotivation;

    @Column(name = "TEAM_COMPETENCE", nullable = false)
    @Enumerated(EnumType.STRING)
    private Rating teamCompetence;

    @Column(name = "SUCCESS_OF_THE_PROJECT", nullable = false)
    @Enumerated(EnumType.STRING)
    private Rating successOfTheProject;

    @Column(name = "CUSTOMER_SATISFACTION", nullable = false)
    @Enumerated(EnumType.STRING)
    private Rating customerSatisfaction;

    @PrePersist
    public void prePersist() {
        this.createDate = LocalDate.now();
    }
    public static enum Rating {
        BAD,
        AVERAGE,
        GOOD;
    }
}
