package ru.pflb.dev.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "SETTING")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Setting {
    @Id
    private Long id;
    private String days;
    private Integer reportDay;
    @Column(name="HOURS")
    private Integer hour;
    private Integer minute;
    private String attentionText;
}