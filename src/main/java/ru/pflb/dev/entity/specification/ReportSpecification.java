package ru.pflb.dev.entity.specification;

import org.springframework.data.jpa.domain.Specification;
import ru.pflb.dev.controller.dto.filter.ReportFilter;
import ru.pflb.dev.entity.*;

import java.time.LocalDate;

import static org.springframework.data.jpa.domain.Specifications.where;

public class ReportSpecification extends BaseSpecification{
    public static Specification<Report> userIdEqual(Long userId) {
        return (root, cq, cb) -> cb.equal(root.join(Report_.user).get(User_.id), userId);
    }

    public static Specification<Report> orderByCreateDateDesc() {
        return (root, criteriaQuery, cb) -> criteriaQuery.orderBy(cb.desc(root.get(Report_.createDate))).getRestriction();
    }

    public static Specification<Report> createSpecificationByFilter(ReportFilter filter) {
        Specification specification = BaseSpecification.conjunction();

        LocalDate from = filter.getFrom();
        if(from != null) {
            specification = where(specification).and(createDateAfterOrEqual(from));
        }
        LocalDate to = filter.getTo();
        if(to != null) {
            specification = where(specification).and(createDateBeforeOrEqual(to));
        }
        Long directionId = filter.getDirectionId();
        if(directionId != null) {
            specification = where(specification).and(directionIdEqual(directionId));

        }
        return specification;
    }

    private static Specification<Report> createDateAfterOrEqual(LocalDate from) {
        return (root, cq, cb) -> cb.greaterThanOrEqualTo(root.get(Report_.createDate), from);
    }
    private static Specification<Report> createDateBeforeOrEqual(LocalDate to) {
        return (root, cq, cb) -> cb.lessThanOrEqualTo(root.get(Report_.createDate), to);
    }

    public static Specification<Report> directionIdEqual(Long directionId) {
        return (root, cq, cb) -> cb.equal(root.join(Report_.project).get(Project_.direction), directionId);
    }
}
