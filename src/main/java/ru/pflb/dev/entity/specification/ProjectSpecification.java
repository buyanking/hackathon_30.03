package ru.pflb.dev.entity.specification;

import org.springframework.data.jpa.domain.Specification;
import ru.pflb.dev.controller.dto.filter.ProjectFilter;
import ru.pflb.dev.entity.Project;
import ru.pflb.dev.entity.Project_;

import java.sql.Date;
import java.time.LocalDate;

import static org.springframework.data.jpa.domain.Specifications.where;

public class ProjectSpecification extends BaseSpecification{
    private static Specification<Project> createDateAfterOrEqual(LocalDate from) {
        return (root, cq, cb) -> cb.greaterThanOrEqualTo(root.get(Project_.startDate), from);
    }
    private static Specification<Project> createDateBeforeOrEqual(LocalDate to) {
        return (root, cq, cb) -> cb.lessThanOrEqualTo(root.get(Project_.startDate), to);
    }
    public static Specification<Project> idEqual(Long id) {
        return (root, cq, cb) -> cb.equal(root.get(Project_.id), id);
    }
    public static Specification<Project> responsibleGroupHasUser(Long userId) {
        return (root, cq, cb) -> root.join(Project_.users).in(userId);
    }
    public static Specification<Project> createSpecificationByFilter(ProjectFilter filter) {

        Specification specification = BaseSpecification.conjunction();

        LocalDate from = filter.getFrom();
        if(from != null) {
            specification = where(specification).and(createDateAfterOrEqual(from));
        }
        LocalDate to = filter.getTo();
        if(to != null) {
            specification = where(specification).and(createDateBeforeOrEqual(to));
        }
        return specification;
    }
}
