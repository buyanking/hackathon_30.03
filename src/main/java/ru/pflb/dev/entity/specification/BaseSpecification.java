package ru.pflb.dev.entity.specification;

import org.springframework.data.jpa.domain.Specification;

public abstract class BaseSpecification {
    public static <T> Specification<T> conjunction() {
        return (r, cq, cb) -> cb.conjunction();
    }
    public static <T> Specification<T> disjunction() {
        return (r, cq, cb) -> cb.disjunction();
    }
}
