package ru.pflb.dev.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;

@Entity(name = "ROLE")
@AllArgsConstructor
@Setter
@Getter
@Immutable
public class Role extends Dictionary{
}
