package ru.pflb.dev.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Babushkin Vitalii on 31.03.2018.
 */
@Entity(name = "PROJECT_CHRONOS")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ProjectChronos implements Serializable {
    @Id
    @SequenceGenerator(
            name = "projectChronos_sequence",
            sequenceName = "projectChronos_sequence"
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "projectChronos_sequence")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "PROJECT_ID_CHRONOS")
    private Integer projectIdChronos;

    @ManyToOne
    @JoinColumn(name = "PROJECT_ID")
    @JsonManagedReference
    private Project project;
}
