package ru.pflb.dev.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pflb.dev.entity.Project;
import ru.pflb.dev.entity.Setting;

@Repository
public interface SettingRepository extends JpaRepository<Setting, Long> {
}
