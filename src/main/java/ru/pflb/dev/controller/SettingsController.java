package ru.pflb.dev.controller;

/**
 * Created by Danila on 02.01.2018.
 */
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.pflb.dev.entity.Setting;
import ru.pflb.dev.entity.Setting;
import ru.pflb.dev.service.SettingService;
import ru.pflb.dev.service.SettingService;

@RestController
@RequestMapping("/api")
@Log4j
@Secured(value = {"ROLE_ADMIN"})
public class SettingsController {

    private final SettingService settingService;

    @Autowired
    public SettingsController(SettingService settingService) {
        this.settingService = settingService;
    }

    @PostMapping("/setting")
    public ResponseEntity<Setting> createSetting(@RequestBody Setting setting) {
        log.debug("REST request to create user {}");
        setting = settingService.save(setting);
        return ResponseEntity.ok(setting);
    }

    @PutMapping("/setting")
    public ResponseEntity<Setting> saveSetting(@RequestBody Setting setting) {
        log.debug("REST request to create user {}");
        setting = settingService.save(setting);
        return ResponseEntity.ok(setting);
    }

    @GetMapping("/setting")
    public ResponseEntity<Setting> getSetting(){
        log.debug("REST request to get users");
        return ResponseEntity.ok(settingService.findAll().stream().findFirst().orElse(null));
    }

}