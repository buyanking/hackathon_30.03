package ru.pflb.dev.controller.dto.filter;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Date;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProjectFilter {
    @DateTimeFormat( pattern = "dd.MM.yyyy")
    private LocalDate from;
    @DateTimeFormat( pattern = "dd.MM.yyyy")
    private LocalDate to;
}
