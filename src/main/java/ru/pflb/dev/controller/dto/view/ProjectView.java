package ru.pflb.dev.controller.dto.view;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import ru.pflb.dev.entity.Project;

import java.time.LocalDate;
import java.util.Optional;

@Data
@Builder
public class ProjectView {
    private Long id;
    private String name;
    private String photoPath;
    @JsonFormat(pattern = "dd.MM.yyyy")
    private LocalDate startDate;
    @JsonFormat(pattern = "dd.MM.yyyy")
    private LocalDate endDate;
    private boolean active;
    private String direction;
    private boolean hasReports;

    public static ProjectView buildInfoFrom(Project entity, boolean hasReports) {
        return Optional.ofNullable(entity).map(project -> {
            return ProjectView.builder()
                    .id(project.getId())
                    .name(project.getName())
                    .direction(project.getDirection().getName())
                    .hasReports(hasReports)
                    .active(project.getStartDate() != null && (project.getEndDate() == null ||
                            project.getStartDate().isBefore(project.getEndDate())))
                    .build();
        }).orElse(null);
    }
}
