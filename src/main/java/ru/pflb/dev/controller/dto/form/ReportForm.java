package ru.pflb.dev.controller.dto.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.pflb.dev.entity.Report;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReportForm {
    @NotNull
    private Long projectId;
    private String comment;
    @NotNull
    private Report.Rating teamMotivation;
    @NotNull
    private Report.Rating teamCompetence;
    @NotNull
    private Report.Rating successOfTheProject;
    @NotNull
    private Report.Rating customerSatisfaction;
}
