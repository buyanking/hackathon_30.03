package ru.pflb.dev.controller.dto.view;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import ru.pflb.dev.entity.Report;

import java.time.LocalDate;
import java.util.Optional;

@Data
@Builder
public class ReportView {
    private Long id;
    private ProjectView project;
    private UserView user;
    private String comment;
    @JsonFormat(pattern = "dd.MM.yyyy")
    private LocalDate createDate;
    private Report.Rating teamMotivation;
    private Report.Rating teamCompetence;
    private Report.Rating successOfTheProject;
    private Report.Rating customerSatisfaction;

    public static ReportView buildFrom(Report entity) {
        return Optional.ofNullable(entity).map(report -> {
            return ReportView.builder()
                    .id(report.getId())
                    .comment(report.getComment())
                    .createDate(report.getCreateDate())
                    .customerSatisfaction(report.getCustomerSatisfaction())
                    .successOfTheProject(report.getSuccessOfTheProject())
                    .teamCompetence(report.getTeamCompetence())
                    .teamMotivation(report.getTeamMotivation())
                    .user(UserView.buildInfoFrom(report.getUser()))
                    .project(ProjectView.buildInfoFrom(report.getProject(), true))
                    .build();
        }).orElse(null);
    }
}
