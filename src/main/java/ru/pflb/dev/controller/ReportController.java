package ru.pflb.dev.controller;

import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.pflb.dev.controller.dto.filter.ReportFilter;
import ru.pflb.dev.controller.dto.form.ReportForm;
import ru.pflb.dev.controller.dto.view.ReportView;
import ru.pflb.dev.entity.Direction;
import ru.pflb.dev.service.ReportService;

import java.time.LocalDate;

@RestController
@RequestMapping("/api")
@Log4j
public class ReportController {

    private final ReportService reportService;

    @Autowired
    public ReportController(ReportService reportService) {
        this.reportService = reportService;
    }

    @PostMapping("/report")
    public ResponseEntity<ReportView> createReport(@RequestBody ReportForm form) {
        log.debug("REST request to create report {}");
        return ResponseEntity.ok(reportService.save(form));
    }

    @GetMapping("/user/report")
    public ResponseEntity<Page<ReportView>> getUserReports(Pageable page){
        log.debug("REST request to get report");
        return ResponseEntity.ok(reportService.loggedUserReportsSortedByDate(page));
    }

    @GetMapping("/user/report/week")
    public ResponseEntity<Page<ReportView>> getUserReports(Pageable page, ReportFilter reportFilter){
        log.debug("REST request to get report");
        if (reportFilter.getFrom() == null) {
            LocalDate fromDate = LocalDate.now().minusDays(7);
            reportFilter.setFrom(fromDate);
        }
        return ResponseEntity.ok(reportService.findAll(reportFilter, page));
    }

    @GetMapping("/report")
    @Secured(value = {"ROLE_ADMIN", "ROLE_LEADER"})
    public ResponseEntity<Page<ReportView>> getReports(ReportFilter filter, Pageable page){
        log.debug("REST request to get report");
        return ResponseEntity.ok(reportService.findAll(filter, page));
    }

    @GetMapping("/latestReport")
    public ResponseEntity<Page<ReportView>> getReportsForEachProject(Pageable page){
        log.debug("REST request to get latest report for each project");
        return ResponseEntity.ok(reportService.getLatestReports(page));
    }
}
