package ru.pflb.dev.controller;

/**
 * Created by Danila on 02.01.2018.
 */
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.pflb.dev.entity.Direction;
import ru.pflb.dev.service.DirectionService;

import java.util.List;

@RestController
@RequestMapping("/api")
@Log4j
public class DirectionController {

    private final DirectionService directionService;

    @Autowired
    public DirectionController(DirectionService directionService) {
        this.directionService = directionService;
    }

    @GetMapping("/direction")
    @Secured(value = {"ROLE_ADMIN", "ROLE_LEADER"})
    public ResponseEntity<List<Direction>> getProjects(){
        log.debug("REST request to get directions");
        return ResponseEntity.ok(directionService.findAll());
    }

}