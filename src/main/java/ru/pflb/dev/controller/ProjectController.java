package ru.pflb.dev.controller;

/**
 * Created by Danila on 02.01.2018.
 */
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.pflb.dev.controller.dto.filter.ProjectFilter;
import ru.pflb.dev.controller.dto.view.ProjectView;
import ru.pflb.dev.entity.Project;
import ru.pflb.dev.service.ProjectService;

import java.util.List;

@RestController
@RequestMapping("/api")
@Log4j
public class ProjectController {

    private final ProjectService projectService;

    @Autowired
    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @PostMapping("/project")
    @Secured(value = {"ROLE_ADMIN", "ROLE_LEADER"})
    public ResponseEntity<Project> createProject(@RequestBody Project project) {
        log.debug("REST request to create project {}");
        project = projectService.save(project);
        return ResponseEntity.ok(project);
    }

    @DeleteMapping("/project/{id}")
    @Secured(value = {"ROLE_ADMIN", "ROLE_LEADER"})
    public ResponseEntity deleteProject(@PathVariable Long id) {
        log.debug("REST request to delete project by id {} ");
        projectService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/project")
    public ResponseEntity<Page<Project>> getProjects(ProjectFilter filter, Pageable page){
        log.debug("REST request to get projects");
        return ResponseEntity.ok(projectService.findAll(filter, page));
    }

    @PostMapping("/project/{projectId}/user/{userId}")
    public ResponseEntity<Project> setProjectExecutor(@PathVariable Long projectId, @PathVariable Long userId) {
        Project project = projectService.setProjectExecutor(projectId, userId);
        return ResponseEntity.ok(project);
    }

    @GetMapping("/myProjects")
    public ResponseEntity<List<ProjectView>> getProjectsForCurrentUser(){
        log.debug("REST request to get user's projects");
        return ResponseEntity.ok(projectService.getCurrentUserProjects());
    }

}