package ru.pflb.dev.service;

import ru.pflb.dev.entity.Direction;

import java.util.List;

public interface DirectionService {
    List<Direction> findAll();

    Direction save(Direction direction);

}

