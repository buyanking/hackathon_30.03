package ru.pflb.dev.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.pflb.dev.controller.dto.filter.ReportFilter;
import ru.pflb.dev.controller.dto.form.ReportForm;
import ru.pflb.dev.controller.dto.view.ReportView;
import ru.pflb.dev.entity.Report;

public interface ReportService {
    ReportView save(ReportForm report);

    Page<ReportView> findAll(ReportFilter filter, Pageable page);

/*
    Page<Report> findAll(Long userId, Pageable page);
*/

    Page<ReportView> loggedUserReportsSortedByDate(Pageable page);

    boolean exists(ReportFilter filter);

    Page<ReportView> getLatestReports(Pageable page);
}
