package ru.pflb.dev.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.pflb.dev.controller.dto.filter.ProjectFilter;
import ru.pflb.dev.controller.dto.view.ProjectView;
import ru.pflb.dev.entity.Project;

import java.util.List;

public interface ProjectService {
    Page<Project> findAll(ProjectFilter filter, Pageable page);

    Project save(Project project);

    void delete(Long id);

    Project getCurrentUserProject(Long projectId);

    Project setProjectExecutor (Long projectId, Long userId);

    List<ProjectView> getCurrentUserProjects();
}
