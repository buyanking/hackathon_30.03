package ru.pflb.dev.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pflb.dev.entity.Direction;
import ru.pflb.dev.repository.DirectionRepository;
import ru.pflb.dev.service.DirectionService;

import java.util.List;

@Service
public class DirectionServiceImpl implements DirectionService {
    private DirectionRepository repository;

    @Autowired
    public DirectionServiceImpl(DirectionRepository repository) {
        this.repository = repository;
    }
    @Override
    public List<Direction> findAll() {
        return this.repository.findAll();
    }

    @Override
    public Direction save(Direction direction) {
        return this.repository.save(direction);
    }
}
