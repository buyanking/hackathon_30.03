package ru.pflb.dev.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.pflb.dev.entity.Setting;
import ru.pflb.dev.repository.SettingRepository;
import ru.pflb.dev.service.SettingService;
import ru.pflb.dev.service.SettingService;

import java.util.List;

@Service
public class SettingServiceImpl implements SettingService {

    private final SettingRepository repository;

    @Autowired
    public SettingServiceImpl(SettingRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Setting> findAll() {
        return repository.findAll();
    }

    @Override
    public Setting save(Setting setting) {
        return repository.save(setting);
    }


}
