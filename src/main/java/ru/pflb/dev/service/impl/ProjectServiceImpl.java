package ru.pflb.dev.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ru.pflb.dev.controller.dto.filter.ProjectFilter;
import ru.pflb.dev.controller.dto.filter.ReportFilter;
import ru.pflb.dev.controller.dto.view.ProjectView;
import ru.pflb.dev.entity.Project;
import ru.pflb.dev.entity.Report;
import ru.pflb.dev.entity.specification.ReportSpecification;
import ru.pflb.dev.entity.User;
import ru.pflb.dev.repository.ProjectRepository;
import ru.pflb.dev.repository.ReportRepository;
import ru.pflb.dev.security.SecurityUtils;
import ru.pflb.dev.service.ProjectService;
import ru.pflb.dev.service.ReportService;

import javax.persistence.EntityNotFoundException;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.data.jpa.domain.Specifications.where;
import static ru.pflb.dev.entity.specification.ProjectSpecification.createSpecificationByFilter;
import static ru.pflb.dev.entity.specification.ProjectSpecification.idEqual;
import static ru.pflb.dev.entity.specification.ProjectSpecification.responsibleGroupHasUser;

@Service
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository repository;
    private final ReportRepository reportRepository;

    @Autowired
    public ProjectServiceImpl(ProjectRepository repository, ReportRepository reportRepository) {
        this.repository = repository;
        this.reportRepository = reportRepository;
    }

    @Override
    public Page<Project> findAll(ProjectFilter filter, Pageable page) {
        return repository.findAll(createSpecificationByFilter(filter), page);
    }

    @Override
    public Project save(Project project) {
        return repository.save(project);
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }

    @Override
    public List<ProjectView> getCurrentUserProjects(){
        List<Project> projects = repository.findAll(responsibleGroupHasUser(SecurityUtils.getCurrentUserId()));

        ReportFilter filter = new ReportFilter();
        filter.setFrom(LocalDate.now().minusDays(7));
        filter.setTo(LocalDate.now());
        Specification<Report> specification = ReportSpecification.createSpecificationByFilter(filter);
        boolean hasReports = reportRepository.count(specification) > 0;

        return projects.stream().map(p -> ProjectView.buildInfoFrom(p, hasReports))
                .collect(Collectors.toList());
    }

    @Override
    public Project getCurrentUserProject(Long projectId) {
        Project project = repository.findOne(where(idEqual(projectId))
                .and(responsibleGroupHasUser(SecurityUtils.getCurrentUserId())));
        if(project == null) {
            throw new EntityNotFoundException("User has no projects with id " + projectId);
        }

        return project;
    }

    @Override
    public Project setProjectExecutor(Long projectId, Long userId) {
        Project project = repository.findOne(projectId);
        project.getUsers().add(User.builder().id(userId).build());
        Project savedProject = repository.save(project);
        return savedProject;
    }
}
