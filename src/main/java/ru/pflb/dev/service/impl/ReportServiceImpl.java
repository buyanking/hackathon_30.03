package ru.pflb.dev.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import ru.pflb.dev.controller.dto.filter.ReportFilter;
import ru.pflb.dev.controller.dto.form.ReportForm;
import ru.pflb.dev.controller.dto.view.ReportView;
import ru.pflb.dev.entity.Project;
import ru.pflb.dev.entity.Report;
import ru.pflb.dev.entity.User;
import ru.pflb.dev.entity.specification.ReportSpecification;
import ru.pflb.dev.repository.ReportRepository;
import ru.pflb.dev.security.SecurityUtils;
import ru.pflb.dev.service.ProjectService;
import ru.pflb.dev.service.ReportService;
import ru.pflb.dev.service.UserService;

import javax.persistence.EntityNotFoundException;

import static org.springframework.data.jpa.domain.Specifications.where;

@Service
public class ReportServiceImpl implements ReportService {

    private ReportRepository repository;
    private ProjectService projectService;
    private UserService userService;

    @Autowired
    public ReportServiceImpl(ReportRepository repository, ProjectService projectService, UserService userService) {
        this.repository = repository;
        this.projectService = projectService;
        this.userService = userService;
    }

    @Override
    public ReportView save(ReportForm form) {
        Report report = Report.builder()
                .project(projectService.getCurrentUserProject(form.getProjectId()))
                .user(userService.findById(SecurityUtils.getCurrentUser().getId()))
                .comment(form.getComment())
                .successOfTheProject(form.getSuccessOfTheProject())
                .customerSatisfaction(form.getCustomerSatisfaction())
                .teamCompetence(form.getTeamCompetence())
                .teamMotivation(form.getTeamMotivation()).build();
        return ReportView.buildFrom(repository.save(report));
    }

    @Override
    public Page<ReportView> findAll(ReportFilter filter, Pageable page) {
        Specification<Report> specification = ReportSpecification.createSpecificationByFilter(filter);
        where(specification).and(ReportSpecification.orderByCreateDateDesc());
        return repository.findAll(specification, page)
                .map(ReportView::buildFrom);
    }

    /*@Override
    public Page<Report> findAll(Long userId, Pageable page) {
        return repository.findAll(ReportSpecification.userIdEqual(userId), page);
    }*/

    @Override
    public Page<ReportView> loggedUserReportsSortedByDate(Pageable page) {
        Long userId = SecurityUtils.getCurrentUserId();
        Specification<Report> specification = ReportSpecification.userIdEqual(userId);
        specification = where(specification).and(ReportSpecification.orderByCreateDateDesc());
        return repository.findAll(specification, page).map(ReportView::buildFrom);
    }

    @Override
    public boolean exists(ReportFilter filter) {
        Specification<Report> specification = ReportSpecification.createSpecificationByFilter(filter);
        return repository.count(specification) > 0;
    }

    @Override
    public Page<ReportView> getLatestReports(Pageable page){
        Specifications<Report> specification = where(ReportSpecification.orderByCreateDateDesc());
        return repository.findAll(specification, page).map(ReportView::buildFrom);
    }
}
