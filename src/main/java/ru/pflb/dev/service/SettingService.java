package ru.pflb.dev.service;

import ru.pflb.dev.entity.Setting;

import java.util.List;

public interface SettingService {
    List<Setting> findAll();

    Setting save(Setting setting);

}
