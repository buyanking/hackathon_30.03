insert into role values (1, 'ADMIN');
insert into role values (2, 'LEADER');
insert into role values (3, 'USER');

insert into users values (1, 'Максим','Максимович', 'Дед', 'ded@mai.ru', 'https://cdn.iconscout.com/public/images/icon/free/png-512/avatar-user-hacker-3830b32ad9e0802c-512x512.png', '777 77 77', 'ADMIN', 1, '$2a$10$.2BXQVx3htAO.prOHDLK6er7Rrtg1xk5lgr.fZ31cp1ncjVQyIvYi');
insert into users values (2, 'Семен','Семенович', 'Карп', 'ded@mai.ru', 'https://cmkt-image-prd.global.ssl.fastly.net/0.1.0/ps/1441771/1160/772/m1/fpnw/wm0/detective-avatar-icon-01-.jpg?1468239271&s=81b626a6572cf3817d9c0194c144aa5b', '777 77 77', 'LEADER', 2, '$2a$10$.2BXQVx3htAO.prOHDLK6er7Rrtg1xk5lgr.fZ31cp1ncjVQyIvYi');
insert into users values (3, 'Иоан','Максимильянович', 'Коровин', 'ded@mai.ru', 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/user-male-icon.png', '777 77 77', 'USER', 3, '$2a$10$.2BXQVx3htAO.prOHDLK6er7Rrtg1xk5lgr.fZ31cp1ncjVQyIvYi');
insert into users values (4, 'Иоан1','Максимильянович', 'Коровин', 'ded@mai.ru', 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/user-male-icon.png', '777 77 77', 'USER1', 3, '$2a$10$.2BXQVx3htAO.prOHDLK6er7Rrtg1xk5lgr.fZ31cp1ncjVQyIvYi');
insert into users values (5, 'Иоан2','Максимильянович', 'Коровин', 'ded@mai.ru', 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/user-male-icon.png', '777 77 77', 'USER2', 3, '$2a$10$.2BXQVx3htAO.prOHDLK6er7Rrtg1xk5lgr.fZ31cp1ncjVQyIvYi');
insert into users values (6, 'Иоан3','Максимильянович', 'Коровин', 'ded@mai.ru', 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/user-male-icon.png', '777 77 77', 'USER3', 3, '$2a$10$.2BXQVx3htAO.prOHDLK6er7Rrtg1xk5lgr.fZ31cp1ncjVQyIvYi');
insert into users values (7, 'Иоан4','Максимильянович', 'Коровин', 'ded@mai.ru', 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/user-male-icon.png', '777 77 77', 'USER4', 3, '$2a$10$.2BXQVx3htAO.prOHDLK6er7Rrtg1xk5lgr.fZ31cp1ncjVQyIvYi');
insert into users values (8, 'Иоан5','Максимильянович', 'Коровин', 'ded@mai.ru', 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/user-male-icon.png', '777 77 77', 'USER5', 3, '$2a$10$.2BXQVx3htAO.prOHDLK6er7Rrtg1xk5lgr.fZ31cp1ncjVQyIvYi');
insert into users values (9, 'Иоан6','Максимильянович', 'Коровин', 'ded@mai.ru', 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/user-male-icon.png', '777 77 77', 'USER6', 3, '$2a$10$.2BXQVx3htAO.prOHDLK6er7Rrtg1xk5lgr.fZ31cp1ncjVQyIvYi');
insert into users values (10, 'Иоан7','Максимильянович', 'Коровин', 'ded@mai.ru', 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/user-male-icon.png', '777 77 77', 'USER7', 3, '$2a$10$.2BXQVx3htAO.prOHDLK6er7Rrtg1xk5lgr.fZ31cp1ncjVQyIvYi');
insert into users values (11, 'Иоан8','Максимильянович', 'Коровин', 'ded@mai.ru', 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/user-male-icon.png', '777 77 77', 'USER8', 3, '$2a$10$.2BXQVx3htAO.prOHDLK6er7Rrtg1xk5lgr.fZ31cp1ncjVQyIvYi');
insert into users values (12, 'Иоан9','Максимильянович', 'Коровин', 'ded@mai.ru', 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/user-male-icon.png', '777 77 77', 'USER9', 3, '$2a$10$.2BXQVx3htAO.prOHDLK6er7Rrtg1xk5lgr.fZ31cp1ncjVQyIvYi');
insert into users values (13, 'Иоан10','Максимильянович', 'Коровин', 'ded@mai.ru', 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/user-male-icon.png', '777 77 77', 'USER10', 3, '$2a$10$.2BXQVx3htAO.prOHDLK6er7Rrtg1xk5lgr.fZ31cp1ncjVQyIvYi');
insert into users values (14, 'Иоан11','Максимильянович', 'Коровин', 'ded@mai.ru', 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/user-male-icon.png', '777 77 77', 'USER11', 3, '$2a$10$.2BXQVx3htAO.prOHDLK6er7Rrtg1xk5lgr.fZ31cp1ncjVQyIvYi');

insert into DIRECTION values (1, 'АТ');
insert into DIRECTION values (2, 'ФТ');
insert into DIRECTION values (3, 'НТ');

insert into PROJECT values (1, 'Chronos', to_date('01.01.2015', 'DD.MM.YYYY'), to_date('01.01.2018', 'DD.MM.YYYY'), 1);
insert into PROJECT values (2, 'TUMEN', to_date('01.02.2015', 'DD.MM.YYYY'), to_date('01.01.2019', 'DD.MM.YYYY'), 2);
insert into PROJECT values (3, 'X5', to_date('01.03.2015', 'DD.MM.YYYY'), to_date('01.01.2020', 'DD.MM.YYYY'), 3);

insert into PROJECT_CHRONOS values (1, 'Chronos_C', 11, 1);
insert into PROJECT_CHRONOS values (2, 'TUMEN_C', 22, 2);
insert into PROJECT_CHRONOS values (3, 'X5_C', 33, 2);

insert into REPORT values (1, 1, 1, to_date('01.01.2015', 'DD.MM.YYYY'), 'все плохо', 'BAD', 'AVERAGE', 'AVERAGE', 'AVERAGE');
insert into REPORT values (2, 1, 2, to_date('01.01.2016', 'DD.MM.YYYY'), 'все еше хуже', 'BAD', 'BAD', 'AVERAGE', 'AVERAGE');
insert into REPORT values (3, 2, 1, to_date('01.01.2017', 'DD.MM.YYYY'), 'ваще пипец', 'BAD', 'BAD', 'BAD', 'AVERAGE');
insert into REPORT values (4, 3, 3, to_date('01.01.2015', 'DD.MM.YYYY'), 'хочу спать', 'BAD', 'BAD', 'BAD', 'BAD');
insert into REPORT values (10, 1, 1, to_date('01.01.2015', 'DD.MM.YYYY'), 'все плохо', 'BAD', 'AVERAGE', 'AVERAGE', 'AVERAGE');
insert into REPORT values (20, 1, 2, to_date('01.01.2016', 'DD.MM.YYYY'), 'все еше хуже', 'BAD', 'BAD', 'AVERAGE', 'AVERAGE');
insert into REPORT values (30, 2, 1, to_date('01.01.2017', 'DD.MM.YYYY'), 'ваще пипец', 'BAD', 'BAD', 'BAD', 'AVERAGE');
insert into REPORT values (40, 3, 3, to_date('01.01.2015', 'DD.MM.YYYY'), 'хочу спать', 'BAD', 'BAD', 'BAD', 'BAD');
insert into REPORT values (11, 1, 1, to_date('01.01.2015', 'DD.MM.YYYY'), 'все плохо', 'BAD', 'AVERAGE', 'AVERAGE', 'AVERAGE');
insert into REPORT values (21, 1, 2, to_date('01.01.2016', 'DD.MM.YYYY'), 'все еше хуже', 'BAD', 'BAD', 'AVERAGE', 'AVERAGE');
insert into REPORT values (31, 2, 1, to_date('01.01.2017', 'DD.MM.YYYY'), 'ваще пипец', 'BAD', 'BAD', 'BAD', 'AVERAGE');
insert into REPORT values (41, 3, 3, to_date('01.01.2015', 'DD.MM.YYYY'), 'хочу спать', 'BAD', 'BAD', 'BAD', 'BAD');

insert into USER_PROJECT values (1, 1, 1, 0);
insert into USER_PROJECT values (2, 2, 2, 1);
insert into USER_PROJECT values (3, 3, 3, 0);
insert into USER_PROJECT values (4, 2, 2, 0);

insert into SETTING values (1, '0001110', 4, 12, 0, 'Пора отправить отчет');